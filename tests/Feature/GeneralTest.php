<?php

namespace Tests\Feature;


use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GeneralTest extends TestCase
{

    public function test_new_users_can_register()
    {
        $response = $this->post('/api/v1/auth/register/', [
            'name' => 'Test User',
            'email' => 'test2@example.com',
            'password' => 'password',
            'password_confirmation' => 'password',
        ]);
        if(isset(json_decode(json_encode($response),1)['baseResponse']['original']['email'])){
            if(json_decode(json_encode($response),1)['baseResponse']['original']['email'][0] == "The email has already been taken."){
                $response->assertStatus(422);
            }
        }
        else{
            $response->assertStatus(201);
        }
    } 
    public function test_users_can_login()
    {
        $response = $this->post('/api/v1/auth/login/', [
            'email' => 'test2@example.com',
            'password' => 'password',
        ]);
        $token = json_decode(json_encode($response),1)['baseResponse']['original']['token'];
        $response->assertStatus(200);
        return $token;
    }

    /**
     * @depends test_users_can_login
     */
     public function test_get_mobil($token)
     {
        $response = $this->get('/api/v1/mobil/',['Authorization' => 'Bearer' . $token]);
        $response->assertStatus(200);
        return $token;
     }

    /**
     * @depends test_get_mobil
     */
     public function test_create_mobil($token)
     {
         $response = $this->post('/api/v1/mobil/', [
            'nama' => 'ferari',
            'tahun_keluaran' => '2033',
            'warna' => 'hitam',
            'harga' => 200000000,
            'mesin' => 'bensin',
            'kapasitas_penumpang' => 7,
            'tipe' => 'Suv',
            'stok' => 7,
        ],['Authorization' => 'Bearer' . $token]);

        $response->assertStatus(200);
        
        return [
            "id"    => json_decode(json_encode($response),1)['baseResponse']['original']['data']['id'],
            "token" => $token
        ];

     }

    /**
     * @depends test_create_mobil
     */
    public function test_get_by_id_mobil($token)
    {
        $response = $this->get('/api/v1/mobil/'.$token['id'],['Authorization' => 'Bearer' . $token['token']]);
        $response->assertStatus(200);
        return $token;
    }

    /**
     * @depends test_get_by_id_mobil
     */
    public function test_update_mobil($token)
    {
        $response = $this->post('/api/v1/mobil/', [
            'nama' => 'ferari',
            'tahun_keluaran' => '2033',
            'warna' => 'hitam',
            'harga' => 200000000,
            'mesin' => 'bensin',
            'kapasitas_penumpang' => 7,
            'tipe' => 'Suv',
            'stok' => 7,
            'id'  => $token['id']
        ],['Authorization' => 'Bearer' . $token['token']]);
        $response->assertStatus(200);
        return $token;
    }

    /**
     * @depends test_update_mobil
     */
    public function test_delete_mobil($token)
    {
        $response = $this->delete('/api/v1/mobil/'.$token['id'],[],['Authorization' => 'Bearer' . $token['token']]);
        $response->assertStatus(200);
        return $token['token'];
    }

    /**
     * @depends test_delete_mobil
     */
    public function test_create_motor($token)
    {
        $response = $this->post('/api/v1/motor/', [
            'nama' => 'Supra',
            'tahun_keluaran' => 2033,
            'warna' => 'hitam',
            'harga' => 20000,
            'mesin' => 'bensin',
            'tipe_suspensi' => 'Monoshock',
            'tipe_transmisi' => 'matic',
            'stok' => 7,
       ],['Authorization' => 'Bearer' . $token]);

       $response->assertStatus(200);
       
       return [
           "id"    => json_decode(json_encode($response),1)['baseResponse']['original']['data']['id'],
           "token" => $token
       ];

    }

    /**
     * @depends test_create_motor
     */
    public function test_get_motor($token)
    {
       $response = $this->get('/api/v1/motor/',['Authorization' => 'Bearer' . $token['token']]);
       $response->assertStatus(200);
       return $token;
    }

   /**
    * @depends test_get_motor
    */
   public function test_get_by_id_motor($token)
   {
       $response = $this->get('/api/v1/motor/'.$token['id'],['Authorization' => 'Bearer' . $token['token']]);
       $response->assertStatus(200);
       return $token;
   }

   /**
    * @depends test_get_by_id_motor
    */
   public function test_update_motor($token)
   {
       $response = $this->post('/api/v1/motor/', [
            'nama' => 'beat',
            'tahun_keluaran' => 2033,
            'warna' => 'hitam',
            'harga' => 20000,
            'mesin' => 'bensin',
            'tipe_suspensi' => 'Monoshock',
            'tipe_transmisi' => 'matic',
            'stok' => 7,
            'id'  => $token['id']
       ],['Authorization' => 'Bearer' . $token['token']]);
       $response->assertStatus(200);
       return $token;
   }

   /**
    * @depends test_update_motor
    */
   public function test_delete_motor($token)
   {
       $response = $this->delete('/api/v1/motor/'.$token['id'],[],['Authorization' => 'Bearer' . $token['token']]);
       $response->assertStatus(200);
       return $token['token'];
   }


    /**
     * @depends test_delete_motor
     */
    public function test_create_penjualan_kendaraan($token)
    {

        $mobil = $this->post('/api/v1/mobil/', [
            'nama' => 'ferari',
            'tahun_keluaran' => '2033',
            'warna' => 'hitam',
            'harga' => 200000000,
            'mesin' => 'bensin',
            'kapasitas_penumpang' => 7,
            'tipe' => 'Suv',
            'stok' => 7,
        ],['Authorization' => 'Bearer' . $token]);
        $mobil = json_decode(json_encode($mobil),1)['baseResponse']['original']['data']['id'];

        $response = $this->post('/api/v1/penjualan-kendaraan/', [
            'kendaraan_id' => $mobil,
            'qty' => '1',
        ],['Authorization' => 'Bearer' . $token]);
        $response->assertStatus(200);
       
       return [
           "id"    => json_decode(json_encode($response),1)['baseResponse']['original']['data']['id'],
           "token" => $token
       ];

    }

    /**
     * @depends test_create_penjualan_kendaraan
     */
    public function test_get_penjualan_kendaraan($token)
    {
       $response = $this->get('/api/v1/penjualan-kendaraan/',['Authorization' => 'Bearer' . $token['token']]);
       $response->assertStatus(200);
       return $token;
    }

   /**
    * @depends test_get_penjualan_kendaraan
    */
   public function test_get_by_id_penjualan_kendaraan($token)
   {
       $response = $this->get('/api/v1/penjualan-kendaraan/'.$token['id'],['Authorization' => 'Bearer' . $token['token']]);
       $response->assertStatus(200);
       return $token;
   }

   /**
    * @depends test_get_by_id_penjualan_kendaraan
    */
   public function test_delete_penjualan_kendaraan($token)
   {
       $response = $this->delete('/api/v1/penjualan-kendaraan/'.$token['id'],[],['Authorization' => 'Bearer' . $token['token']]);
       $response->assertStatus(200);
       return $token['token'];
   }
}
