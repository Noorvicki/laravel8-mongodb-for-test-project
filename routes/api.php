<?php

use App\Http\Controllers\Api\Kendaraan\MobilController;
use App\Http\Controllers\Api\Kendaraan\MotorController;
use App\Http\Controllers\Api\Penjualan\PenjualanKendaraanController;
use App\Http\Controllers\Api\User\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::prefix('v1')->group(function () {
    /**
     * Auth
     */
    Route::prefix('auth')->group(function () {
        Route::post('/login',  [AuthController::class,'login'])->name("login");
        Route::post('/register', [AuthController::class,'register'])->name("register");
        Route::post('/logout', [AuthController::class,'logout'])->name("logout");
        Route::post('/refresh', [AuthController::class,'refresh'])->name("refresh");
    });
    Route::middleware('jwt')->group(function () {

        /**
         * CRUD Mobil
         */
        Route::get('/mobil/', [MobilController::class, 'index']);
        Route::get('/mobil/{id}', [MobilController::class, 'show']);
        Route::post('/mobil/', [MobilController::class, 'store']);
        Route::put('/mobil', [MobilController::class, 'update']);
        Route::delete('/mobil/{id}', [MobilController::class, 'destroy']);
            
        /**
         * CRUD Motor
         */
        Route::get('/motor', [MotorController::class, 'index']);
        Route::get('/motor/{id}', [MotorController::class, 'show']);
        Route::post('/motor', [MotorController::class, 'store']);
        Route::put('/motor', [MotorController::class, 'update']);
        Route::delete('/motor/{id}', [MotorController::class, 'destroy']);

        /**
         * CRUD Penjualan
         */
        Route::get('/penjualan-kendaraan', [PenjualanKendaraanController::class, 'index']);
        Route::get('/penjualan-kendaraan/{id}', [PenjualanKendaraanController::class, 'show']);
        Route::post('/penjualan-kendaraan', [PenjualanKendaraanController::class, 'store']);
        Route::put('/penjualan-kendaraan', [PenjualanKendaraanController::class, 'update']);
        Route::delete('/penjualan-kendaraan/{id}', [PenjualanKendaraanController::class, 'destroy']);

        /**
         * Laporan Penjualan
         */
        Route::get('/laporan-penjualan', [PenjualanKendaraanController::class, 'laporanPenjualan']);
    });

});


Route::get('/', function () {
    return response()->failed(['Endpoint yang anda minta tidak tersedia']);
});

Route::fallback(function () {
    return response()->failed(['Endpoint yang anda minta tidak dapat diakses']);
});