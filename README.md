
# Laravel 8 Mongodb - for test project

Merupakan mini project menggunakan laravel 8 dan mongodb, serta menerapkan konsep Single Table Inheritance


## Installation

- Clone project :

    ```bash
    git clone https://gitlab.com/Noorvicki/laravel8-mongodb-for-test-project.git
    ```
- Masuk ke folder project :
    ```bash
    cd ./laravel8-mongodb-for-test-project
    ```
- Install composer package :
    ```bash
    composer install
    ```

## Configuration

Duplikat `.env.example` dan ubah nama menjadi `.env`


Lakukan konfigurasi database pada bagian berikut :
```bash
DB_CONNECTION=mongodb
DB_HOST=127.0.0.1
DB_PORT=27017
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=
```


## Run The Project

Jalankan program dengan perintah 

```bash
php artisan serve
```

secara default program akan dijalankan pada url sebagai berikut

```bash
http://127.0.0.1:8000/
```

## Api Documentation

api documentation berupa export collection & environment postman yang dapat diimport pada `postman.zip` dan pastikan seluruh endpoint menggunakan `Bearer Token Authorization` kecuali `/register & /login` 

## Test

Unit test dapat dijalankan dengan perintah

```bash
php artisan test
```
