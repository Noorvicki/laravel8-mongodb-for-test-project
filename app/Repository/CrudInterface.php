<?php 
namespace App\Repository;

Interface CrudInterface
{
    public function getAll(array $filter,int $itemPerPage, bool $paginate): object;

    public function getById(string $id);

    public function create(array $payload): array;

    public function update(array $payload, string $id): array;

    public function delete(string $id): Bool;
}