<?php

namespace App\Http\Controllers\Api\Kendaraan;

use App\Helpers\Kendaraan\MobilHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Kendaraan\KendaraanRequest;
use App\Http\Resources\Kendaraan\MobilCollection;
use App\Http\Resources\Kendaraan\MobilResource;
use Illuminate\Http\Request;

class MobilController extends Controller
{
    protected $mobil;
    public function __construct()
    {
        $this->mobil = new MobilHelper();
    }
    public function index(Request $request)
    {
        $filter = [
            'nama' => $request->nama ?? '',
        ];

        $itemPerPage = $request->itemPerPage ?? 0;
        $mobil = $this->mobil->getAll($filter, $itemPerPage);
        return response()->success(new MobilCollection($mobil));
    }

    public function show($id)
    {
        $dataMobil = $this->mobil->getById($id);
        if (isset($dataMobil->error)) {
            return response()->failed(['Data Mobil tidak ditemukan']);
        }

        return response()->success(new MobilResource($dataMobil));
    }

    public function store(KendaraanRequest $request)
    {
        if (isset($request->validator) && $request->validator->fails()) {
            return response()->failed($request->validator->errors(), 422);
        }
        
        $dataInput = $request->only(['nama','tahun_keluaran','warna','harga','mesin','kapasitas_penumpang','tipe','stok']);

        $dataMobil = $this->mobil->create($dataInput);

        if(!$dataMobil['status']) {
            return response()->failed($dataMobil['error'], 422);
        }

        return response()->success(new MobilResource($dataMobil['data']), 'Data Mobil berhasil disimpan');
    }

    public function update(KendaraanRequest $request)
    {
        if (isset($request->validator) && $request->validator->fails()) {
            return response()->failed($request->validator->errors(), 422);
        }
        $dataInput = $request->only(['nama','tahun_keluaran','warna','harga','mesin','kapasitas_penumpang','tipe','stok']);
        $dataMobil = $this->mobil->update($dataInput, (string)$request['id']);

        if(!$dataMobil['status']) {
            return response()->failed('Tidak dapat mengubah data mobil', 422);
        }

        return response()->success(new MobilResource($dataMobil['data']), 'Data Mobil berhasil diupdate');
    }

    public function destroy($id)
    {
        $dataMobil = $this->mobil->delete($id);

        if (!$dataMobil) {
            return response()->failed(['Mohon maaf data Mobil tidak ditemukan']);
        }

        return response()->success($dataMobil, 'Data Mobil telah dihapus');
    }
}
