<?php

namespace App\Http\Controllers\Api\Kendaraan;

use App\Helpers\Kendaraan\MotorHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Kendaraan\KendaraanRequest;
use App\Http\Resources\Kendaraan\MotorCollection;
use App\Http\Resources\Kendaraan\MotorResource;
use Illuminate\Http\Request;

class MotorController extends Controller
{
    protected $motor;
    public function __construct()
    {
        $this->motor = new MotorHelper();
    }
    public function index(Request $request)
    {
        $filter = [
            'nama' => $request->nama ?? '',
        ];
        
        $itemPerPage = $request->itemPerPage ?? 0;
        $motor = $this->motor->getAll($filter, $itemPerPage);
        return response()->success(new MotorCollection($motor));
    }

    public function show($id)
    {
        $dataMotor = $this->motor->getById($id);
        if (isset($dataMotor->error)) {
            return response()->failed(['Data Motor tidak ditemukan']);
        }

        return response()->success(new MotorResource($dataMotor));
    }

    public function store(KendaraanRequest $request)
    {
        if (isset($request->validator) && $request->validator->fails()) {
            return response()->failed($request->validator->errors(), 422);
        }
        
        $dataInput = $request->only(['nama','tahun_keluaran','warna','harga','mesin','kapasitas_penumpang','tipe','stok']);

        $dataMotor = $this->motor->create($dataInput);

        if(!$dataMotor['status']) {
            return response()->failed($dataMotor['error'], 422);
        }

        return response()->success(new MotorResource($dataMotor['data']), 'Data Motor berhasil disimpan');
    }

    public function update(KendaraanRequest $request)
    {
        if (isset($request->validator) && $request->validator->fails()) {
            return response()->failed($request->validator->errors(), 422);
        }
        $dataInput = $request->only(['nama','tahun_keluaran','warna','harga','mesin','kapasitas_penumpang','tipe','stok']);
        $dataMotor = $this->motor->update($dataInput, (string)$request['id']);

        if(!$dataMotor['status']) {
            return response()->failed('Tidak dapat mengubah data motor', 422);
        }

        return response()->success(new MotorResource($dataMotor['data']), 'Data Motor berhasil diupdate');
    }

    public function destroy($id)
    {
        $dataMotor = $this->motor->delete($id);

        if (!$dataMotor) {
            return response()->failed(['Mohon maaf data Motor tidak ditemukan']);
        }

        return response()->success($dataMotor, 'Data Motor telah dihapus');
    }
}
