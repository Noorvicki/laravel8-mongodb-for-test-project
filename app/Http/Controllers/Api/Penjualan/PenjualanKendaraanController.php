<?php

namespace App\Http\Controllers\Api\Penjualan;

use App\Helpers\Penjualan\PenjualanKendaraanHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Penjualan\PenjualanKendaraanRequest;
use App\Http\Resources\Penjualan\PenjualanKendaraanCollection;
use App\Http\Resources\Penjualan\PenjualanKendaraanResource;
use Illuminate\Http\Request;

class PenjualanKendaraanController extends Controller
{
    protected $penjualan;
    public function __construct()
    {
        $this->penjualan = new PenjualanKendaraanHelper();
    }
    public function index(Request $request)
    {
        $filter = [
            'kendaraan_id' => $request->kendaraan_id ?? '',
        ];
        $itemPerPage = $request->itemPerPage ?? 0;
        $penjualan = $this->penjualan->getAll($filter, $itemPerPage);
        return response()->success(new PenjualanKendaraanCollection($penjualan));
    }

    public function show($id)
    {
        $dataPenjualanKendaraan = $this->penjualan->getById($id);
        if (isset($dataPenjualanKendaraan->error)) {
            return response()->failed(['Data Penjualan Kendaraan tidak ditemukan']);
        }

        return response()->success(new PenjualanKendaraanResource($dataPenjualanKendaraan));
    }

    public function store(PenjualanKendaraanRequest $request)
    {
        if (isset($request->validator) && $request->validator->fails()) {
            return response()->failed($request->validator->errors(), 422);
        }
        
        $dataInput = $request->only(['kendaraan_id','qty']);
        
        $dataPenjualanKendaraan = $this->penjualan->create($dataInput);

        if(!$dataPenjualanKendaraan['status']) {
            return response()->failed($dataPenjualanKendaraan['error'], 422);
        }

        return response()->success(new PenjualanKendaraanResource($dataPenjualanKendaraan['data']), 'Data Penjualan Kendaraan berhasil disimpan');
    }

    public function update(PenjualanKendaraanRequest $request)
    {
        if (isset($request->validator) && $request->validator->fails()) {
            return response()->failed($request->validator->errors(), 422);
        }
        $dataInput = $request->only(['nama','tahun_keluaran','warna','harga','mesin','kapasitas_penumpang','tipe','stok']);
        $dataPenjualanKendaraan = $this->penjualan->update($dataInput, (string)$request['id']);

        if(!$dataPenjualanKendaraan['status']) {
            return response()->failed('Tidak dapat mengubah data penjualan', 422);
        }

        return response()->success(new PenjualanKendaraanResource($dataPenjualanKendaraan['data']), 'Data Penjualan Kendaraan berhasil diupdate');
    }

    public function destroy($id)
    {
        $dataPenjualanKendaraan = $this->penjualan->delete($id);

        if (!$dataPenjualanKendaraan) {
            return response()->failed(['Mohon maaf data Penjualan Kendaraan tidak ditemukan']);
        }

        return response()->success($dataPenjualanKendaraan, 'Data Penjualan Kendaraan telah dihapus');
    }


    public function laporanPenjualan(Request $request)
    {
        if (!isset($request->jenis)) {
            return response()->failed(['Masukkan opsi jenis']);
        }

        $filter = [
            'tahun' => $request->tahun ?? date("Y"),
            'start_date' => !empty($request->tahun) ? $request->tahun.'-01-01': date('Y-01-01'),
            'end_date' => !empty($request->tahun) ? $request->tahun.'-12-31' : date('Y-12-31'),  
        ];

        $penjualan = $this->penjualan->laporanPenjualan($filter, $request->jenis);

        return response()->success($penjualan);
        
    }
}

