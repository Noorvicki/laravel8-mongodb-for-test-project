<?php

namespace App\Http\Resources\Kendaraan;

use Illuminate\Http\Resources\Json\JsonResource;

class MotorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                 => $this->resource->_id ?? "",
            'nama'               => $this->resource->nama ?? "",
            'tahun_keluaran'     => $this->resource->tahun_keluaran ?? 0000,
            'warna'              => $this->resource->warna ?? "",
            'mesin'              => $this->resource->mesin ?? "",
            'tipe_suspensi'      => $this->resource->tipe_suspensi ?? "",
            'tipe_transmisi'     => $this->resource->tipe_transmisi ?? "",
            'stok'               => $this->resource->stok ?? 0,
        ];
    }
}
