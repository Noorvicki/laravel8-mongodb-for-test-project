<?php

namespace App\Http\Resources\Penjualan;

use Illuminate\Http\Resources\Json\JsonResource;

class PenjualanKendaraanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'               => $this->resource->_id ?? "",
            'kode_transaksi'   => $this->resource->kode_transaksi ?? "",
            'kendaraan_id'     => $this->resource->kendaraan_id ?? "",
            'nama_kendaraan'     => $this->resource->kendaraan->nama ?? "",
            'qty'              => $this->resource->qty ?? 0,
            'total_harga'      => $this->resource->total_harga ?? 0,
        ];
    }
}
