<?php

namespace App\Helpers\Penjualan;

use App\Helpers\Kendaraan\MobilHelper;
use App\Helpers\Kendaraan\MotorHelper;
use App\Models\Kendaraan\KendaraanModel;
use App\Models\Penjualan\PenjualanKendaraanModel;
use App\Repository\CrudInterface;
use Carbon\Carbon;

class PenjualanKendaraanHelper implements CrudInterface
{
    private $penjualanModel, $kendaraanModel, $mobilHelper, $motorHelper;

    public function __construct()
    {
        $this->penjualanModel = new PenjualanKendaraanModel();
        $this->kendaraanModel = new KendaraanModel();
        $this->mobilHelper = new MobilHelper();
        $this->motorHelper = new MotorHelper();
    }

    public function getAll(array $filter, int $itemPerPage = 0, bool $paginate = true ): object
    {
        $penjualan = $this->penjualanModel::query();
        if (!empty($filter['kode_transaksi'])) {
            $penjualan->where('kode_transaksi', '=', $filter['kode_transaksi']);
        }
        if (!empty($filter['start_date']) && !empty($filter['end_date'])) {
            $penjualan->whereBetween('created_at',[$filter['start_date'],$filter['end_date']]);
        }

        if($paginate){
            $itemPerPage = ($itemPerPage > 0) ? $itemPerPage : false ;
            return $penjualan->paginate($itemPerPage);
        }else{
            return $penjualan->get();
        }
    }

    public function getById(string $id): object
    {
        try {
            return $this->penjualanModel::find($id);
        } catch (\Throwable $th) {
            return (object) [
                'error' => $th->getMessage()
            ];
        }
    }

    public function create(array $payload): array
    {

        $kendaraan = $this->kendaraanModel::find($payload['kendaraan_id']);
        
        if($kendaraan == null || ($kendaraan['stok'] < $payload['qty'])){
            return [
                'status' => false,
                'error' => "Stok tidak tersedia"
            ];
        }
        $payload['total_harga'] = $payload['qty'] * $kendaraan['harga'];
        $payload['kode_transaksi'] = 'TRX-'.time()."-".substr($payload['kendaraan_id'],0,4);
        
        try {
            $penjualan = $this->penjualanModel::create($payload);

            $updatedKendaraan = collect($kendaraan)->except("jenis","updated_at","created_at")->toArray();
            $updatedKendaraan["stok"] = ($kendaraan['stok'] - $payload['qty']);

            if(\Str::contains(($kendaraan['jenis']), 'Mobil')){
                $kendaraan = $this->mobilHelper->update($updatedKendaraan, $updatedKendaraan['_id']);
            }elseif(\Str::contains(($kendaraan['jenis']), 'Motor')){
                $kendaraan = $this->motorHelper->update($updatedKendaraan, $updatedKendaraan['_id']);
            }

            return [
                'status' => true,
                'data' => $penjualan
            ];
        } catch (\Throwable $th) {
            return [
                'status' => false,
                'error' => $th->getMessage()
            ];
        }
    }

    public function update(array $payload, string $id): array
    {

        try {
            $penjualan = $this->penjualanModel::find($id);
            $penjualan->update($payload);
            return [
                'status' => true,
                'data' => $this->getById($id)
            ];
        } catch (\Throwable $th) {
            return [
                'status' => false,
                'error' => $th->getMessage()
            ];
        }
    }

    public function delete(string $id): bool
    {
        try {
            $this->penjualanModel::find($id)->delete();
            return true;
        } catch (\Throwable $th) {
            return false;
        }
    }
    public function laporanPenjualan($filter, $jenis)
    {
        
        $filter['start_date'] = new Carbon($filter['start_date']);
        $filter['end_date'] = new Carbon(date("Y-m-d", strtotime($filter['end_date'] . "+1 day")));
        $dataTransaksi = collect(json_decode(json_encode($this->getAll($filter,0,false)),1))->groupBy('kendaraan_id');
        $penjualan=[];
        foreach($dataTransaksi as $perkendaraan){
            $first = true;
            $tempPenjualan=[];
            foreach($perkendaraan as $value){

                $bulan= (int) date('m',strtotime($value['created_at']));
                $model = new PenjualanKendaraanModel($value);

                if(\Str::contains(strtolower($model->kendaraan->jenis),strtolower($jenis))){
                    if($first){
                        $tempTransaksi = [
                            "nama"=>$model->kendaraan->nama,
                            'bulanan' => [
                                    "1" => ["qty" => 0, "total_harga" => 0],
                                    "2" => ["qty" => 0, "total_harga" => 0],
                                    "3" => ["qty" => 0, "total_harga" => 0],
                                    "4" => ["qty" => 0, "total_harga" => 0],
                                    "5" => ["qty" => 0, "total_harga" => 0],
                                    "6" => ["qty" => 0, "total_harga" => 0],
                                    "7" => ["qty" => 0, "total_harga" => 0],
                                    "8" => ["qty" => 0, "total_harga" => 0],
                                    "9" => ["qty" => 0, "total_harga" => 0],
                                    "10" => ["qty" => 0, "total_harga" => 0],
                                    "11" => ["qty" => 0, "total_harga" => 0],
                                    "12" => ["qty" => 0, "total_harga" => 0]
                            ]
                        ];
                        $tempPenjualan = $tempTransaksi;
                        $tempPenjualan['bulanan'][$bulan]["qty"]+= $value['qty'];
                        $tempPenjualan['bulanan'][$bulan]["total_harga"]+= $value['total_harga'];

                    }else{
                        $tempPenjualan['bulanan'][$bulan]["qty"]+= $value['qty'];
                        $tempPenjualan['bulanan'][$bulan]["total_harga"]+= $value['total_harga'];
                    }
                }
                $first = false;
            }
            if(!empty($tempPenjualan))array_push($penjualan,$tempPenjualan);
        }
        
        return [
            "data" => $penjualan,
            "meta" => [
                "title" => "Laporan Penjualan Kendaraan Jenis ".strtoupper($jenis)." Perbulan Tahun " . $filter["tahun"]
            ],
        ];
    }
}
