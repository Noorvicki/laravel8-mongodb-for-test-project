<?php

namespace App\Helpers\Kendaraan;

use App\Models\Kendaraan\MotorModel;
use App\Repository\CrudInterface;

class MotorHelper implements CrudInterface
{
    private $motorModel;

    public function __construct()
    {
        $this->motorModel = new MotorModel();
    }

    public function getAll(array $filter, int $itemPerPage = 0, bool $paginate = true ): object
    {
        $motor = $this->motorModel::query();
        if (!empty($filter['nama'])) {
            $motor->where('nama', 'LIKE', '%'.$filter['nama'].'%');
        }

        if($paginate){
            $itemPerPage = ($itemPerPage > 0) ? $itemPerPage : false ;
            return $motor->paginate($itemPerPage);
        }else{
            return $motor->get();
        }
    }

    public function getById(string $id): object
    {
        try {
            return $this->motorModel::find($id);
        } catch (\Throwable $th) {
            return (object) [
                'error' => $th->getMessage()
            ];
        }
    }

    public function create(array $payload): array
    {
        try {

            $motor = $this->motorModel::create($payload);
            return [
                'status' => true,
                'data' => $motor
            ];
        } catch (\Throwable $th) {
            return [
                'status' => false,
                'error' => $th->getMessage()
            ];
        }
    }

    public function update(array $payload, string $id): array
    {

        try {
            $motor = $this->motorModel::find($id);
            $motor->update($payload);
            return [
                'status' => true,
                'data' => $this->getById($id)
            ];
        } catch (\Throwable $th) {
            return [
                'status' => false,
                'error' => $th->getMessage()
            ];
        }
    }

    public function delete(string $id): bool
    {
        try {
            $this->motorModel::find($id)->delete();
            return true;
        } catch (\Throwable $th) {
            return false;
        }
    }
}
