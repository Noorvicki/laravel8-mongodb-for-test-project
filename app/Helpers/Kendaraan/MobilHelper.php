<?php

namespace App\Helpers\Kendaraan;

use App\Models\Kendaraan\MobilModel;
use App\Repository\CrudInterface;

class MobilHelper implements CrudInterface
{
    private $mobilModel;

    public function __construct()
    {
        $this->mobilModel = new MobilModel();
    }

    public function getAll(array $filter, int $itemPerPage = 0, bool $paginate = true ): object
    {
        $mobil = $this->mobilModel::query();

        if (!empty($filter['nama'])) {
            $mobil->where('nama', 'LIKE', '%'.$filter['nama'].'%');
        }
        if($paginate){
            $itemPerPage = ($itemPerPage > 0) ? $itemPerPage : false ;
            return $mobil->paginate($itemPerPage);
        }else{
            return $mobil->get();
        }
    }

    public function getById(string $id): object
    {
        try {
            return $this->mobilModel::find($id);
        } catch (\Throwable $th) {
            return (object) [
                'error' => $th->getMessage()
            ];
        }
    }

    public function create(array $payload): array
    {
        try {

            $mobil = $this->mobilModel::create($payload);
            return [
                'status' => true,
                'data' => $mobil
            ];
        } catch (\Throwable $th) {
            return [
                'status' => false,
                'error' => $th->getMessage()
            ];
        }
    }

    public function update(array $payload, string $id): array
    {

        try {
            $mobil = $this->mobilModel::find($id);
            $mobil->update($payload);
            return [
                'status' => true,
                'data' => $this->getById($id)
            ];
        } catch (\Throwable $th) {
            return [
                'status' => false,
                'error' => $th->getMessage()
            ];
        }
    }

    public function delete(string $id): bool
    {
        try {
            $this->mobilModel::find($id)->delete();
            return true;
        } catch (\Throwable $th) {
            return false;
        }
    }
}
