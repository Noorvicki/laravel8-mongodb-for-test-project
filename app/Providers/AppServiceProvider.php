<?php

namespace App\Providers;

use Illuminate\Support\Facades\Response;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('success', function ($data = [], $message = '', $settings = []) {
            return Response::make([
                'status_code' => 200,
                'data' => $data,
                'message' => $message,
                'settings' => $settings
            ], 200);
        });

        Response::macro('failed', function ($error = [], $httpCode = 422, $settings = []) {
            if(is_array($error)) {
                $arrError = $error;
            } else {
                $arrError = [];
                $tmpError = (array) $error;
                foreach($tmpError as $val) {
                    foreach((array) $val as $v) {
                        if($v !== ':message') {
                            $arrError[] = $v;
                        }
                    } 
                }
            }

            return Response::make([
                'status_code' => $httpCode,
                'errors' => $arrError,
                'settings' => $settings
            ], $httpCode);
        });
        if (!Collection::hasMacro('paginate')) {

            Collection::macro('paginate', 
                function ($perPage = 15, $page = null, $options = []) {
                $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
                return (new LengthAwarePaginator(
                    $this->forPage($page, $perPage)->values()->all(), $this->count(), $perPage, $page, $options))
                    ->withPath('');
            });
        }
    }
}
