<?php

namespace App\Models\Penjualan;

use App\Models\Kendaraan\KendaraanModel;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class PenjualanKendaraanModel extends Eloquent
{
    use SoftDeletes;

    protected $collection = 'penjualan_kendaraan';
    protected $fillable = [
        'kode_transaksi',
        'kendaraan_id',
        'qty',
        'total_harga',
    ];
    public function kendaraan(){
        return $this->belongsTo(KendaraanModel::class);
    }
}