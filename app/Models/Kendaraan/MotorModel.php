<?php

namespace App\Models\Kendaraan;


class MotorModel extends KendaraanModel
{
    // protected $fillable = [];
    public function __construct(array $attributes = [])
    {
        $fillable = ['mesin','tipe_suspensi','tipe_transmisi','stok'];
        $this->fillable = array_merge($fillable, $this->fillable);
        parent::__construct($attributes);
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function ($motor) {
            $motor->forceFill(['jenis' => self::class]);
        });
        
    }

    public static function booted()
    {
        static::addGlobalScope('motor',  function ($builder) {
            $builder->where('jenis', self::class);
        });
    }
}