<?php

namespace App\Models\Kendaraan;

class MobilModel extends KendaraanModel
{
    // protected $fillable = [];
    public function __construct(array $attributes = [])
    {
        $fillable = ['mesin','kapasitas_penumpang','tipe','stok'];
        $this->fillable = array_merge($fillable, $this->fillable);
        parent::__construct($attributes);
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function ($mobil) {
            $mobil->forceFill(['jenis' => self::class]);
        });
        
    }

    public static function booted()
    {
        static::addGlobalScope('mobil',  function ($builder) {
            $builder->where('jenis', self::class);
        });
    }
}