<?php

namespace App\Models\Kendaraan;

use App\Models\Penjualan\PenjualanKendaraanModel;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Collection;
class KendaraanModel extends Eloquent
{
    use SoftDeletes;

    protected $collection = 'kendaraan';
    protected $fillable = [
        'nama',
        'tahun_keluaran',
        'warna',
        'harga',
    ];

    public function penjualanKendaraan(){
        return $this->hasMany(PenjualanKendaraanModel::class);
    }
}